﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ArrAlg
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private async void btnFind_Click(object sender, EventArgs e)
        {
            await Find(GetFirst(), GetSecond());
        }

        private int[] GetFirst()
        {
            return Utils.GetIntArr(tbFirst.Text.Split(','));
        }

        private int[] GetSecond()
        {
            return Utils.GetIntArr(tbSecond.Text.Split(','));
        }

        private async Task Find(int[] firstSeq, int[] secondSeq)
        {
            WriteLog(firstSeq, secondSeq);
            var res = await Utils.FindCrossing(firstSeq, secondSeq);
            WriteLog(res);
        }

        private void WriteLog(int[] firstSeq, int[] secondSeq)
        {
            rtbOutput.Clear();
            rtbOutput.Text += "You intered:\n";
            rtbOutput.Text += $"First sequence: [{string.Join(", ", firstSeq)}]\n";
            rtbOutput.Text += $"Second sequence: [{string.Join(", ", secondSeq)}]";
        }

        private void WriteLog(int[] resultSeq)
        {
            rtbOutput.Text += "\n\n";
            rtbOutput.Text += "--------------------------------\n";
            rtbOutput.Text += $"Result: [{string.Join(", ", resultSeq)}]";
        }
    }

    public class Utils
    {
        public static int[] GetIntArr(string[] val)
        {
            return val.Select(x => int.Parse(x)).ToArray();
        }

        public async static Task<int[]> FindCrossing(int[] firstSeq, int[] secondSeq)
        {
            return await Task.Run(() => 
            {
                //Решение тут
                List<int> result = new List<int>();
                int stopIndex = 0;

                foreach (var x in firstSeq)
                {
                    for (int y = stopIndex; y < secondSeq.Length; y++)
                    {
                        if (x == secondSeq[y])
                        {
                            stopIndex = y;
                            result.Add(x);
                            break;
                        }
                    }
                }

                result.Sort();
                return result.ToArray();
            });
        }
    }
}
